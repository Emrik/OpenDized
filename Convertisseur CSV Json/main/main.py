#!/usr/bin/python
# -*- coding:Utf-8 -*-
'''
Created on 17 août 2016
@note: au sujet de l'encodage et des caratères: "\xef\xbb\xbf"
    http://stackoverflow.com/questions/18664712/split-function-add-xef-xbb-xbf-n-to-my-list
@author: rbeck
'''
import os
import csv
import json

if __name__ == '__main__':
    
#initialisation de variable
    fileUrl = "../confCSV.csv"
    fileJsonUrl= "../config.json"
    
    listRows = list()
    listJson = list()
    
#lecture du fichier CSV
    with open(fileUrl,"rb") as datasCSV:
        #lecture de format CSV Excel
        datas= csv.reader(datasCSV, delimiter=';')#on peut ajouter " quotechar='"' " pour un délimiter de texte tel que: ' " '
        for data in datas:
            listRows.append(data)
    
#traitement du fichier CSV
#on va avoir une liste avec des dictionnaire à l'interieur!
    listForJson = list()
    dictForStages = dict()
    listForRows = list()
    dictForRow = dict()
    
    cptStages = 0#compteur de phases
    
    for row in listRows:
        if "@title" in row[0]:
        #nous avons un début de phase
            if cptStages > 0:
            #nous ne somme pas au début donc nous devont "enregistrer l'objet
            #et "formater" un nouvel objet "phase"
                dictForStages["rows"]=listForRows
                listForJson.append(dictForStages)
                dictForStages = dict()
                dictForRow = dict()
                listForRows = []
            dictForStages["title"]=row[1]
        # verifier dans quelle phase nous sommes
            if dictForStages["title"]=="ButDuJeu" or\
            dictForStages["title"]=="MiseEnPlace" or\
            dictForStages["title"]=="PhaseDeJeu" or\
            dictForStages["title"]=="LaPartieSeTermine" or\
            dictForStages["title"]=="ConditionVictoire" or\
            dictForStages["title"]=="Precisions":             
                pass #rien à faire pour l instant
        # sinon le dernier fichier n'est pas récupéré,
        #     à cause de la sauvegarde en début de formatage du nouvel objet phase... (après "if cptStages > 0:")
            elif dictForStages["title"]=="EOF":
                print "fin de fichier atteind"
            else:
                print "erreur de titre dans le fichier CSV"
                '''
@ICI
    vérifier le code ci dessus
'''
            cptStages += 1
        else:
        #nous avons donc une ligne
            dictForRow["desc"]=row[0]
            dictForRow["videoPath"]=row[1]
            dictForRow["tags"]=row[2]
            listForRows.append(dictForRow)
            dictForRow = dict()
    print "lecture du fichier fini!"
    
    '''
    ' Passage de l'objet vers le Json
    '''
    objJson = json.dumps(listForJson)
    print "passage en Json OK"
    
    '''
    ' Ecriture du fichier avec les éléments Json
    '''
    #ouverture du fichier en écriture
    with open(fileJsonUrl,"wb") as datasJson:
        datasJson.write("var conf ='")
        datasJson.write(objJson)
        datasJson.write("';")
    print "Fichier écrit"